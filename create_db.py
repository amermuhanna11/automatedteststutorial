# beginning of create_db.py
import json
from models import app, db, Book

def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

def create_books():
    book = load_json('books.json')

    for oneBook in book['Books']:
        title = oneBook['title']
        id = oneBook['book_id']
        google_id = oneBook['google_id']
        isbn = oneBook['isbn']
        publication_date = oneBook['publication_date']
        book_image_url = oneBook['image_url']
        book_description = oneBook['description']

        publisher_name = oneBook['publishers'][0]['name']
        publisher_wiki_url = oneBook['publishers'][0]['wikipedia_url']
        publisher_description = oneBook['publishers'][0]['description']
        publisher_image_url = oneBook['publishers'][0]['image_url']
        publisher_website = oneBook['publishers'][0]['website']
        
        author_name = oneBook['authors'][0]['name']
        author_born = oneBook['authors'][0]['born']
        author_nationality = oneBook['authors'][0]['nationality']
        author_description = oneBook['authors'][0]['description']
        author_alma_mater = oneBook['authors'][0]['alma_mater']
        author_wikipedia_url = oneBook['authors'][0]['wikipedia_url']
        author_image_url = oneBook['authors'][0]['image_url']

        newBook = Book(title = title, id = id, book_description = book_description, isbn = isbn, book_image_url = book_image_url, publisher_name = publisher_name, author_name = author_name, publication_date = publication_date, google_id = google_id, author_born = author_born, author_description = author_description, author_nationality = author_nationality, author_alma_mater = author_alma_mater, author_image_url = author_image_url, author_wikipedia_url = author_wikipedia_url, publisher_description = publisher_description, publisher_image_url = publisher_image_url, publisher_website = publisher_website, publisher_wiki_url = publisher_wiki_url)
        
        # After I create the book, I can then add it to my session. 
        db.session.add(newBook)
        # commit the session to my DB.
        db.session.commit()
	
create_books()
# end of create_db.py
