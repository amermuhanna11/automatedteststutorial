# beginning of main3.py
from flask import Flask, render_template, request
#from models import  app, db, Book
from create_db import app, db, Book, create_books
from sqlalchemy import desc, func


@app.route('/')
def index():
	return render_template('hello1.html')

@app.route('/books/')
def books():
	books = db.session.query(Book).all()
	return render_template('dynamicBooks.html', books = books)

@app.route('/authors/')
def authors():
	books = db.session.query(Book).all()
	author_set = set()
	return render_template('dynamicAuthors.html', books = books, author_set = author_set)

@app.route('/publishers/')
def publishers():
	books = db.session.query(Book).all()
	publisher_set = set()
	author_set = set()
	return render_template('dynamicPublishers.html', books = books, author_set = author_set, publisher_set = publisher_set)

@app.route('/about/')
def about():
	return render_template('about.html')

@app.route('/books/<int:variable>', methods=['GET'])
def individual_book(variable):
	s = db.session.query(Book).filter_by(id = variable).one()
	return render_template("dynamicIndividualBooks.html", id = variable, author_name = s.author_name, publisher_name = s.publisher_name, title = s.title, publication_date = s.publication_date, isbn = s.isbn, book_image_url = s.book_image_url, google_id = s.google_id, book_description = s.book_description)

@app.route('/authors/<int:variable>', methods=['GET'])
def individual_author(variable):
	books = db.session.query(Book).all()
	s = db.session.query(Book).filter_by(id = variable).one()
	publisher_set = set()
	return render_template("dynamicIndividualAuthors.html",books = books, id = variable, author_name = s.author_name, publisher_set = publisher_set, publisher_name = s.publisher_name, title = s.title, publication_date = s.publication_date, isbn = s.isbn, book_image_url = s.book_image_url, author_born = s.author_born, author_nationality = s.author_nationality, author_description= s.author_description, author_alma_mater = s.author_alma_mater, author_wikipedia_url = s.author_wikipedia_url, author_image_url = s.author_image_url)

@app.route('/publishers/<int:variable>', methods=['GET'])
def individual_publisher(variable):
	books = db.session.query(Book).all()
	s = db.session.query(Book).filter_by(id = variable).one()
	author_set = set()
	return render_template("dynamicIndividualPublishers.html", id = variable, author_set = author_set, publisher_image_url = s.publisher_image_url, publisher_name = s.publisher_name, publisher_description = s.publisher_description, publisher_website = s.publisher_website, publisher_wiki_url = s.publisher_wiki_url, books = books)

@app.route('/test/')
def test():
	return render_template('test.html')

@app.route('/searchresults/', methods = ['GET', 'POST'])
def searchresults():
	
	author_set = set()
	book_set = set()
	publisher_set = set()
	variable = request.form.get("variable")
	variable = variable.lower()
	

	reg_exp = list(variable)
	
	'''
	return_str = ''
	for char in reg_exp:
		return_str += '(' 
		return_str += char
		return_str += '+'
		return_str += ')'
	'''
	#variable = return_str
	
	variable = '%' + variable + '%'
	book_query = db.session.query(Book).filter(func.lower(Book.title).like(variable)).all()
	author_query = db.session.query(Book).filter(func.lower(Book.author_name).like(variable)).all()
	publisher_query = db.session.query(Book).filter(func.lower(Book.publisher_name).like(variable)).all()

	if len(book_query) == 0 and len(author_query) == 0 and len(publisher_query) == 0:
		return render_template('noSearchResults.html', variable = request.form.get("variable"))
	else:
		return render_template('searchResults.html', book = book_query, author = author_query, publisher = publisher_query, author_set = author_set, book_set = book_set, publisher_set = publisher_set)

if __name__ == "__main__":
	app.run()
# end of main3.py



