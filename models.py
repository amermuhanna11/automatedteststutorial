# beginning of models.py
# note that at this point you should have created "bookdb" database (see install_postgres.txt).
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
import sqlalchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:password@localhost:5432/bookdb')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

class Book(db.Model):
	__tablename__ = 'book'
	
	title = db.Column(db.String(800), nullable = True)
	assert isinstance(title, sqlalchemy.sql.schema.Column)
	id = db.Column(db.Integer, primary_key = True)
	assert isinstance(id, sqlalchemy.sql.schema.Column)
	isbn = db.Column(db.String(800), nullable = True)
	assert isinstance(isbn, sqlalchemy.sql.schema.Column)
	book_image_url = db.Column(db.String(800), nullable = True)
	assert isinstance(book_image_url, sqlalchemy.sql.schema.Column)
	book_description = db.Column(db.String(4000), nullable = True)
	assert isinstance(book_description, sqlalchemy.sql.schema.Column)
	publisher_name = db.Column(db.String(800), nullable = True)
	assert isinstance(publisher_name, sqlalchemy.sql.schema.Column)
	author_name = db.Column(db.String(800), nullable = True)
	assert isinstance(author_name, sqlalchemy.sql.schema.Column)
	publication_date = db.Column(db.String(800), nullable = True)
	assert isinstance(publication_date, sqlalchemy.sql.schema.Column)
	google_id = db.Column(db.String(800), nullable = True)
	assert isinstance(google_id, sqlalchemy.sql.schema.Column)
	author_born = db.Column(db.String(800), nullable = True)
	assert isinstance(author_born, sqlalchemy.sql.schema.Column)
	author_nationality = db.Column(db.String(800), nullable = True)
	assert isinstance(author_nationality, sqlalchemy.sql.schema.Column)
	author_description = db.Column(db.String(4000), nullable = True)
	assert isinstance(author_description, sqlalchemy.sql.schema.Column)
	author_alma_mater = db.Column(db.String(800), nullable = True)
	assert isinstance(author_alma_mater, sqlalchemy.sql.schema.Column)
	author_wikipedia_url = db.Column(db.String(800), nullable = True)
	assert isinstance(author_wikipedia_url, sqlalchemy.sql.schema.Column)
	author_image_url = db.Column(db.String(800), nullable = True)
	assert isinstance(author_image_url, sqlalchemy.sql.schema.Column)
	publisher_website = db.Column(db.String(800), nullable = True)
	assert isinstance(publisher_website, sqlalchemy.sql.schema.Column)
	publisher_wiki_url = db.Column(db.String(800), nullable = True)
	assert isinstance(publisher_wiki_url, sqlalchemy.sql.schema.Column)
	publisher_image_url = db.Column(db.String(800), nullable = True)
	assert isinstance(publisher_image_url, sqlalchemy.sql.schema.Column)
	publisher_description = db.Column(db.String(4000), nullable = True)
	assert isinstance(publisher_description, sqlalchemy.sql.schema.Column)

db.drop_all()
db.create_all()
# End of models.py
