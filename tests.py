import os
import sys
import unittest
#from models import db, Book
from create_db import db, Book

class DBTestCases(unittest.TestCase):

#####Books#####
    def test_book_insert_1(self):
        s = Book(id='60', title = 'C++')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '60').one()
        self.assertEqual(str(r.id), '60')

        db.session.query(Book).filter_by(id = '60').delete()
        db.session.commit()

    def test_book_insert_2(self):
        s = Book(id='70', title = 'python')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '70').one()
        self.assertEqual(str(r.title), 'python')

        db.session.query(Book).filter_by(id = '70').delete()
        db.session.commit()


    def test_book_insert_3(self):
        s = Book(id='80', title = 'java', isbn='9781781100486')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '80').one()
        self.assertEqual(str(r.isbn), '9781781100486')

        db.session.query(Book).filter_by(id = '80').delete()
        db.session.commit()
    
    # amers unit tests 
    def test_book_insert_4(self):
        s = Book(id='51', title = 'C')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '51').one()
        self.assertEqual(str(r.id), '51')

        db.session.query(Book).filter_by(id = '51').delete()
        db.session.commit()

    def test_book_insert_5(self):
        s = Book(id='52', title = 'Bio')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '52').one()
        self.assertEqual(str(r.title), 'Bio')

        db.session.query(Book).filter_by(id = '52').delete()
        db.session.commit()
    
    def test_book_insert_6(self):
        s = Book(id='53', title = 'chem', isbn='123456')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Book).filter_by(id = '53').one()
        self.assertEqual(str(r.title), 'chem')

        db.session.query(Book).filter_by(id = '53').delete()
        db.session.commit()

    def test_book_insert_7(self):
        s = Book(id='90', title = 'Ozma of Oz', book_image_url='https://upload.wikimedia.org/wikipedia/commons/2/2a/Ozbook03cover.jpg')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Book).filter_by(id = '90').one()
        self.assertEqual(str(r.book_image_url), 'https://upload.wikimedia.org/wikipedia/commons/2/2a/Ozbook03cover.jpg')

        db.session.query(Book).filter_by(id = '90').delete()
        db.session.commit()

    def test_book_insert_8(self):
        s = Book(id='100', title = 'The Woggle-Bug', book_description='Descriptor for book here.')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Book).filter_by(id = '100').one()
        self.assertEqual(str(r.book_description), 'Descriptor for book here.')

        db.session.query(Book).filter_by(id = '100').delete()
        db.session.commit()

    def test_book_insert_9(self):
        s = Book(id='110', title = 'Wizards Hall', google_id='fOyECgAAQBAJ')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Book).filter_by(id = '110').one()
        self.assertEqual(str(r.google_id), 'fOyECgAAQBAJ')

        db.session.query(Book).filter_by(id = '110').delete()
        db.session.commit()



    def test_book_insert_10(self):
        s = Book(id='54', title = 'physics')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '54').one()
        self.assertEqual(str(r.id), '54')

        db.session.query(Book).filter_by(id = '54').delete()
        db.session.commit()

    def test_book_insert_11(self):
        s = Book(id='55', title = 'ochem')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '55').one()
        self.assertEqual(str(r.title), 'ochem')

        db.session.query(Book).filter_by(id = '55').delete()
        db.session.commit()

    def test_book_insert_12(self):
        s = Book(id='555', publication_date = '11-01-2000')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '555').one()
        self.assertEqual(str(r.publication_date), '11-01-2000')

        db.session.query(Book).filter_by(id = '555').delete()
        db.session.commit()

#####Authors######
    def test_author_insert_1(self):
        s = Book(id='51', author_name = 'Parnav')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '51').one()
        self.assertEqual(str(r.author_name), 'Parnav')

        db.session.query(Book).filter_by(id = '51').delete()
        db.session.commit()


    def test_author_insert_2(self):
        s = Book(id='61', author_name = 'Robison', author_nationality='English')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '61').one()
        self.assertEqual(str(r.author_nationality), 'English')

        db.session.query(Book).filter_by(id = '61').delete()
        db.session.commit()


    def test_author_insert_3(self):
        s = Book(id='71', author_name = 'Silva', author_description='Big description')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '71').one()
        self.assertEqual(str(r.author_description), 'Big description')

        db.session.query(Book).filter_by(id = '71').delete()
        db.session.commit()

    def test_author_insert_4(self):
        s = Book(id='81', author_name = 'Marcus Zusak', author_alma_mater='University of New South Wales')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '81').one()
        self.assertEqual(str(r.author_alma_mater), 'University of New South Wales')

        db.session.query(Book).filter_by(id = '81').delete()
        db.session.commit()

    def test_author_insert_5(self):
        s = Book(id='91', author_name = 'Scott Westerfeld', author_wikipedia_url='https://en.wikipedia.org/wiki/Scott_Westerfeld')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '91').one()
        self.assertEqual(str(r.author_wikipedia_url), 'https://en.wikipedia.org/wiki/Scott_Westerfeld')

        db.session.query(Book).filter_by(id = '91').delete()
        db.session.commit()

    def test_author_insert_6(self):
        s = Book(id='101', author_name = 'Angela Davis', author_image_url='https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Angela_Davis_en_Bogot%C3%A1%2C_Septiembre_de_2010.jpg/800px-Angela_Davis_en_Bogot%C3%A1%2C_Septiembre_de_2010.jpg')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '101').one()
        self.assertEqual(str(r.author_image_url), 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Angela_Davis_en_Bogot%C3%A1%2C_Septiembre_de_2010.jpg/800px-Angela_Davis_en_Bogot%C3%A1%2C_Septiembre_de_2010.jpg')

        db.session.query(Book).filter_by(id = '101').delete()
        db.session.commit()

    def test_author_insert_7(self):
        s = Book(id='211', author_name = 'P.G. Wodehouse', author_description = 'author description for wodehouse')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '211').one()
        self.assertEqual(str(r.author_description), 'author description for wodehouse')

        db.session.query(Book).filter_by(id = '211').delete()
        db.session.commit()
        
    def test_author_insert_7(self):
        s = Book(id='311', author_name = 'Dante', author_nationality = 'Italian')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '311').one()
        self.assertEqual(str(r.author_nationality), 'Italian')

        db.session.query(Book).filter_by(id = '311').delete()
        db.session.commit()

####Publishers####
    def test_publisher_insert_1(self):
        s = Book(id='55', publisher_name='Brandon')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '55').one()
        self.assertEqual(str(r.publisher_name), 'Brandon')

        db.session.query(Book).filter_by(id = '55').delete()
        db.session.commit()

    def test_publisher_insert_2(self):
        s = Book(id='66', publisher_description='Big description')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '66').one()
        self.assertEqual(str(r.publisher_description), 'Big description')

        db.session.query(Book).filter_by(id = '66').delete()
        db.session.commit()


    def test_publisher_insert_3(self):
        s = Book(id='77', publisher_image_url='http://upload.wikimedia.org/wikipedia/en/thumb/d/db/Simon_and_Schuster.svg/200px-Simon_and_Schuster.svg.png')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Book).filter_by(id = '77').one()
        self.assertEqual(str(r.publisher_image_url), 'http://upload.wikimedia.org/wikipedia/en/thumb/d/db/Simon_and_Schuster.svg/200px-Simon_and_Schuster.svg.png')

        db.session.query(Book).filter_by(id = '77').delete()
        db.session.commit()

    def test_publisher_insert_4(self):
        s = Book(id='88', publisher_name='Arbor House', publisher_wiki_url= 'https://en.wikipedia.org/wiki/Arbor_House')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '88').one()
        self.assertEqual(str(r.publisher_wiki_url), 'https://en.wikipedia.org/wiki/Arbor_House')

        db.session.query(Book).filter_by(id = '88').delete()
        db.session.commit()

    def test_publisher_insert_5(self):
        s = Book(id='99', publisher_name='Black Dog Publishing', publisher_website= 'http://www.blackdogonline.com/')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '99').one()
        self.assertEqual(str(r.publisher_website), 'http://www.blackdogonline.com/')

        db.session.query(Book).filter_by(id = '99').delete()
        db.session.commit()

    def test_publisher_insert_6(self):
        s = Book(id='199', publisher_name='Black Dog Publishing', publisher_website= 'http://www.blackdogonline.com/')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '199').one()
        self.assertEqual(str(r.publisher_website), 'http://www.blackdogonline.com/')

        db.session.query(Book).filter_by(id = '199').delete()
        db.session.commit()

    def test_publisher_insert_7(self):
        s = Book(id='299', publisher_name='Black Dog Publishing', publisher_website= 'http://www.blackdogonline.com/')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '299').one()
        self.assertEqual(str(r.publisher_website), 'http://www.blackdogonline.com/')

        db.session.query(Book).filter_by(id = '299').delete()
        db.session.commit()





        

if __name__ == '__main__':
    unittest.main()
